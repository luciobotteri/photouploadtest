//
//  Photo.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 02/06/21.
//

import UIKit
import Photos

enum PhotoState {
    case notUploaded, inUpload, uploaded, error
}

struct Photo: Identifiable {
    let id: UUID
    let asset: PHAsset?
    var image: UIImage
    var state = PhotoState.notUploaded {
        didSet {
            if state != .error {
                error = nil
            }
        }
    }
    var url: URL?
    var isUploadedSuccessfully: Bool {
        return state == .uploaded && url != nil
    }
    var error: String?
}
