//
//  Country.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 30/05/21.
//

import Foundation

struct Country: Codable, Comparable {
    let iso: Int
    let isoAlpha2: String
    let isoAlpha3: String
    let name: String
    let phonePrefix: String
    var emojiFlag: String {
        let base: UInt32 = 127397
        var s = ""
        for v in isoAlpha2.unicodeScalars {
            if let unicodeScalar = UnicodeScalar(base + v.value) {
                s.unicodeScalars.append(unicodeScalar)
            }
        }
        return String(s)
    }
    
    static func < (lhs: Country, rhs: Country) -> Bool {
        return lhs.name < rhs.name
    }
}

struct CountriesResponse: Codable {
    let countries: [Country]
}

struct CountriesSection {
    let letter: String
    let countries: [Country]
}
