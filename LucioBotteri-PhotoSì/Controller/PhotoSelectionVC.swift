//
//  PhotosUploadVC.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 31/05/21.
//

import UIKit
import BSImagePicker
import Photos

class PhotoSelectionVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet private weak var photosCollectionView: UICollectionView!
    @IBOutlet private weak var addPhotosButton: UIButton!
    @IBOutlet private weak var sharePhotosButton: UIButton!
    
    private var editButton: UIBarButtonItem? {
        return navigationItem.rightBarButtonItem
    }
    private let cameraPicker = UIImagePickerController()
    private var photos: [Photo] {
        return DataManager.shared.photos
    }
    
    private var state = PhotoCellState.normal {
        didSet {
            let newButton = UIBarButtonItem(barButtonSystemItem: state == .edit ? .done : .edit, target: self, action: #selector(editButtonAction(_:)))
            newButton.isEnabled = !photos.isEmpty
            navigationItem.setRightBarButton(newButton, animated: true)
            photosCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            cameraPicker.delegate = self
            cameraPicker.sourceType = .camera
            cameraPicker.allowsEditing = true
        }
        setButtonStyle(for: addPhotosButton, sharePhotosButton)
        NotificationCenter.default.addObserver(self, selector: #selector(photosDidChange), name: .photosDidChange, object: nil)
        photosDidChange()
        setupLongPressGestureRecognizer()
    }
    
    @IBAction func addPhotosButtonAction(_ sender: UIButton) {
        state = .normal
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = DataManager.photoLimit-photos.count
        let alertStyle: UIAlertController.Style = UIDevice.current.userInterfaceIdiom == .phone ? .actionSheet : .alert
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: alertStyle)
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            alert.addAction(UIAlertAction(title: "Gallery", style: .default) { [self] _ in
                presentImagePicker(
                    imagePicker,
                    select: nil,
                    deselect: nil,
                    cancel: nil,
                    finish: { assets in
                        for asset in assets {
                            PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: nil) { (image, info) in
                                if let image = image {
                                    if let i = photos.firstIndex(where: { $0.asset == asset }) {
                                        DataManager.shared.photos[i].image = image
                                        photosCollectionView.reloadItems(at: [IndexPath(item: i, section: 0)])
                                    } else {
                                        DataManager.shared.photos.append(Photo(id: UUID(), asset: asset, image: image))
                                    }
                                }
                            }
                        }
                        photosCollectionView.reloadData()
                    })
            })
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alert.addAction(UIAlertAction(title: "Camera", style: .default) { [self] _ in
                present(cameraPicker, animated: true)
            })
        }
        
        alert.addAction(UIAlertAction(title: "Maybe later", style: .cancel))
        present(alert, animated: true)
    }
    
    @IBAction func sharePhotosButtonAction(_ sender: UIButton) {
        NetworkManager().uploadAllImages()
        performSegue(withIdentifier: "toURLsVC", sender: self)
    }
    
    @IBAction func editButtonAction(_ sender: UIBarButtonItem) {
        state.toggle()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage ?? info[.originalImage] as? UIImage {
            DataManager.shared.photos.append(Photo(id: UUID(), asset: nil, image: image))
            photosCollectionView.reloadData()
        }
        dismiss(animated: true)
    }
    
    @objc private func photosDidChange() {
        if photos.isEmpty {
            state = .normal
        }
        var shareButtonTitle = "Proceed to upload \(photos.count) photo"
        if photos.count != 1 {
            shareButtonTitle += "s"
        }
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.editButton?.isEnabled = !self.photos.isEmpty
            self.addPhotosButton.isEnabled = self.photos.count < DataManager.photoLimit
            self.addPhotosButton.alpha = self.addPhotosButton.isEnabled ? 1 : 0.5
            self.addPhotosButton.backgroundColor = self.addPhotosButton.isEnabled ? .systemBlue : .systemGray
            self.sharePhotosButton.setTitle(shareButtonTitle, for: .normal)
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
                self.sharePhotosButton.isHidden = self.photos.isEmpty
                self.sharePhotosButton.alpha = self.photos.isEmpty ? 0 : 1
            }
        }
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        guard state == .normal, gestureRecognizer.state == .began else {
            return
        }
        let p = gestureRecognizer.location(in: photosCollectionView)
        if photosCollectionView.indexPathForItem(at: p) != nil {
            UIImpactFeedbackGenerator(style: .medium).impactOccurred()
            state = .edit
        }
    }
    
    private func setupLongPressGestureRecognizer() {
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        lpgr.delegate = self
        lpgr.minimumPressDuration = 0.3
        lpgr.delaysTouchesBegan = true
        photosCollectionView.addGestureRecognizer(lpgr)
    }
    
    private func setButtonStyle(for buttons: UIButton...) {
        buttons.forEach {
            $0.titleLabel?.adjustsFontSizeToFitWidth = true
            $0.titleLabel?.adjustsFontForContentSizeCategory = true
            $0.titleLabel?.font = .preferredFont(forTextStyle: UIFont.TextStyle.body)
            $0.layer.cornerRadius = $0.frame.height/2
        }
    }
}

extension PhotoSelectionVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if photos.count == 0 {
            let backgroundImageView = UIImageView(image: UIImage(named: "down-arrow"))
            backgroundImageView.contentMode = .center
            collectionView.backgroundView = backgroundImageView
        } else {
            collectionView.backgroundView = nil
        }
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionViewCell
        cell.configure(with: photos[indexPath.item].image)
        cell.state = state
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if state == .edit {
            DataManager.shared.photos.remove(at: indexPath.item)
            collectionView.reloadData()
        } else {
            let vc = UIViewController()
            let imageView = UIImageView(image: photos[indexPath.item].image)
            imageView.contentMode = .scaleAspectFit
            vc.view.backgroundColor = .systemBackground
            vc.view.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.centerXAnchor.constraint(equalTo: vc.view.centerXAnchor).isActive = true
            imageView.centerYAnchor.constraint(equalTo: vc.view.centerYAnchor).isActive = true
            imageView.widthAnchor.constraint(equalTo: vc.view.widthAnchor).isActive = true
            imageView.heightAnchor.constraint(equalTo: vc.view.heightAnchor).isActive = true
            present(vc, animated: true)
        }
    }
}

extension PhotoSelectionVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let width = view.frame.width/2 - flowLayout.sectionInset.left*1.5
            return CGSize(width: width, height: width)
        }
        return CGSize(width: 150, height: 150)
    }
}
