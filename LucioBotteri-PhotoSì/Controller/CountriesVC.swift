//
//  CountriesVC.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 30/05/21.
//

import UIKit

class CountriesVC: UIViewController {
    
    @IBOutlet weak var countriesTableView: UITableView!
    
    var countries: [Country] {
        return DataManager.shared.countries
    }
    var sections = [CountriesSection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkManager().getCountries { [weak self] _ in
            DispatchQueue.main.async {
                let groupedDictionary = Dictionary(grouping: DataManager.shared.countries, by: { String($0.name.prefix(1)) })
                let keys = groupedDictionary.keys.sorted()
                self?.sections = keys.map { CountriesSection(letter: $0, countries: groupedDictionary[$0]!.sorted()) }
                self?.countriesTableView.reloadData()
            }
        }
    }
}

extension CountriesVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath)
        let section = sections[indexPath.section]
        let country = section.countries[indexPath.row]
        cell.textLabel?.text = country.emojiFlag + " " + country.name
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.adjustsFontForContentSizeCategory = true
        cell.textLabel?.font = .preferredFont(forTextStyle: UIFont.TextStyle.body)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let section = sections[indexPath.section]
//        let country = section.countries[indexPath.row]
        performSegue(withIdentifier: "toPhotosUploadVC", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sections.map { $0.letter }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections.isEmpty ? nil : sections[section].letter
    }
}
