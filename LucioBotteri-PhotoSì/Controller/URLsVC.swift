//
//  URLsVC.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 02/06/21.
//

import UIKit

class URLsVC: UIViewController {
    var photos: [Photo] {
        return DataManager.shared.photos
    }
}

extension URLsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell", for: indexPath) as! UploadTableViewCell
        cell.configure(with: photos[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
