//
//  NetworkManager.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 30/05/21.
//

import Foundation

enum NetworkError: Error {
    case noImageData, badURL, noData, generic(error: Error)
}

final class NetworkManager {
    
    private let photoforseURL = "https://api.photoforse.online/"
    private let apiKey = "AIzaSyCccmdkjGe_9Yt-INL2rCJTNgoS4CXsRDc"
    
    private let catboxURL = "https://catbox.moe/user/api.php"
    private let userHash = "cd79619f2ed3220c90adde38d"
    
    func getCountries(completion: @escaping (Bool) -> Void) {
        guard let url = URL(string: photoforseURL + "geographics/countries/") else {
            completion(false)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue(apiKey, forHTTPHeaderField: "x-api-key")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                completion(false)
                return
            }
            do {
                let countries = try JSONDecoder().decode([Country].self, from: data)
                countries.forEach {
                    DataManager.shared.countries.append($0)
                }
                completion(true)
            } catch {
                print("Failed to load: \(error.localizedDescription)")
                completion(false)
            }
        }
        
        task.resume()
    }
    
    func uploadAllImages() {
        DataManager.shared.photos
            .filter { !$0.isUploadedSuccessfully }
            .map(\.id)
            .forEach { id in
                uploadOneImage(id: id)
            }
    }
    
    func uploadOneImage(id: UUID) {
        guard let i = DataManager.shared.photos.firstIndex(where: { $0.id == id }) else { return }
        DataManager.shared.photos[i].state = .inUpload
        uploadImage(id) {
            result in
            do {
                let resultString = try result.get()
                if let url = URL(string: resultString) {
                    DataManager.shared.photos[i].url = url
                    DataManager.shared.photos[i].state = .uploaded
                    NotificationCenter.default.post(name: .photoUploadComplete, object: self, userInfo: ["id": id])
                } else {
                    NotificationCenter.default.post(name: .photoUploadComplete, object: self, userInfo: ["id": id, "error": resultString])
                }
            } catch {
                DataManager.shared.photos[i].state = .error
                NotificationCenter.default.post(name: .photoUploadComplete, object: self, userInfo: ["id": id, "error": error.localizedDescription])
            }
        }
    }
    
    private func uploadImage(_ id: UUID,_ completion: @escaping(Result<String, NetworkError>) -> ()) {
        guard let i = DataManager.shared.photos.firstIndex(where: { $0.id == id }),
              let jpegData = DataManager.shared.photos[i].image.jpegData(compressionQuality: 0.4) else {
            completion(.failure(.noImageData))
            return
        }
        
        let filename = id.uuidString + ".jpeg"
        
        let fieldName = "reqtype"
        let fieldValue = "fileupload"
        
        let fieldName2 = "userhash"
        let fieldValue2 = userHash
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // Set the URLRequest to POST and to the specified URL
        guard let baseURL = URL(string: catboxURL) else {
            completion(.failure(.badURL))
            return
        }
        var urlRequest = URLRequest(url: baseURL)
        urlRequest.httpMethod = "POST"
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(id.uuidString)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        
        // Add the reqtype field and its value to the raw http request data
        data.append("\r\n--\(id.uuidString)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue)".data(using: .utf8)!)
        
        // Add the userhash field and its value to the raw http request data
        data.append("\r\n--\(id.uuidString)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue2)".data(using: .utf8)!)
        
        // Add the image data to the raw http request data
        data.append("\r\n--\(id.uuidString)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"fileToUpload\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(jpegData)
        
        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
        data.append("\r\n--\(id.uuidString)--\r\n".data(using: .utf8)!)
        
        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            
            if let error = error {
                completion(.failure(.generic(error: error)))
                return
            }
            
            guard let responseData = responseData else {
                completion(.failure(.noData))
                return
            }
            
            if let responseString = String(data: responseData, encoding: .utf8) {
                completion(.success(responseString))
            }
        }).resume()
    }
}
