//
//  PhotoCollectionViewCell.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 31/05/21.
//

import UIKit

enum PhotoCellState {
    case normal, edit
    mutating func toggle() {
        self = self == .normal ? .edit : .normal
    }
}

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    var state = PhotoCellState.normal {
        didSet {
            deleteButton.isHidden = state == .normal
        }
    }
    
    func configure(with image: UIImage?) {
        photoImageView.image = image
    }
}
