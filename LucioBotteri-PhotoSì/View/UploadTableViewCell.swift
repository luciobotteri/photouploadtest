//
//  UploadTableViewCell.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 02/06/21.
//

import UIKit

class UploadTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var openURLButton: UIButton!
    @IBOutlet weak var reloadButton: UIButton!
    
    private var photoID: UUID?
    private var photo: Photo? {
        return DataManager.shared.photos.first { $0.id == photoID }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLabel(statusLabel, copyButton.titleLabel, openURLButton.titleLabel, reloadButton.titleLabel)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: .photoUploadComplete, object: nil)
    }
    
    @IBAction func copyButtonDidPress(_ sender: UIButton) {
        UIPasteboard.general.string = photo?.url?.absoluteString
        statusLabel.text = "Copied! ✅"
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1300)) { [weak self] in
            self?.refreshUI()
        }
    }
    
    @IBAction func openURLButtonDidPress(_ sender: UIButton) {
        guard let url = photo?.url else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func reloadButtonDidPress(_ sender: UIButton) {
        guard let id = photoID else { return }
        NetworkManager().uploadOneImage(id: id)
        refreshUI()
    }
    
    @objc func notificationReceived(_ notification: NSNotification) {
        guard let id = notification.userInfo?["id"] as? UUID, id == photoID else {
            return
        }
        if let i = DataManager.shared.photos.firstIndex(where: { $0.id == id }) {
            DataManager.shared.photos[i].error = notification.userInfo?["error"] as? String
        }
        DispatchQueue.main.async { [weak self] in
            self?.refreshUI()
        }
    }
    
    func configure(with photo: Photo) {
        photoID = photo.id
        photoImageView.image = photo.image
        refreshUI()
    }
    
    func refreshUI() {
        guard let photo = photo else { return }
        switch photo.state {
        case .notUploaded:
            statusLabel.text = "Pending..."
            loadingView.stopAnimating()
            copyButton.isHidden = true
            openURLButton.isHidden = true
            reloadButton.isHidden = true
        case .inUpload:
            statusLabel.text = "Uploading..."
            loadingView.startAnimating()
            copyButton.isHidden = true
            openURLButton.isHidden = true
            reloadButton.isHidden = true
        case .uploaded:
            statusLabel.text = "Upload successful!"
            loadingView.stopAnimating()
            copyButton.isHidden = false
            openURLButton.isHidden = false
            reloadButton.isHidden = true
        case .error:
            loadingView.stopAnimating()
            statusLabel.text = photo.error ?? "Error! Please try again"
            copyButton.isHidden = true
            openURLButton.isHidden = true
            reloadButton.isHidden = false
        }
        photoImageView.alpha = loadingView.isAnimating ? 0.3 : 1
    }
    
    private func setupLabel(_ labels: UILabel?...) {
        labels.forEach {
            $0?.adjustsFontForContentSizeCategory = true
            $0?.font = .preferredFont(forTextStyle: UIFont.TextStyle.body)
        }
    }
}
