//
//  DataManager.swift
//  LucioBotteri-PhotoSì
//
//  Created by Lucio Botteri on 30/05/21.
//

import Foundation

final class DataManager {
    static let shared = DataManager()
    static let photoLimit = 6
    
    private init() {}
    
    var countries = [Country]()
    var photos = [Photo]() {
        didSet {
            NotificationCenter.default.post(Notification(name: .photosDidChange))
        }
    }
    var urls = [String]()
}

extension Notification.Name {
    static let photosDidChange = Notification.Name(rawValue: "photosDidChange")
    static let photoUploadComplete = Notification.Name(rawValue: "photoUploadComplete")
}
